#!/bin/python3

class Compteur:
    """Cette classe possède un attribut de lcasse qui s'incrémente à chaque fois que l'on crée un objet de ce type"""

    objets_crees = 0 # Le compteur vaut 0 au départ
    def __init__(self):
        """A chaque fois qu'on crée un objet, on incrémente le compteur"""
        Compteur.objets_crees += 1
    def combien(cls):
        """Méthode de classe affichant combien d'objets ont été créés"""
        print("Jusqu'à présent, {} objets ont été créés.".format(cls.objets_crees))

    combien = classmethod(combien)

c=Compteur()
print(Compteur.objets_crees)
b=Compteur()
print(Compteur.objets_crees)
Compteur.combien()
d=Compteur()
Compteur.combien()
print(Compteur.objets_crees)
d.combien()
c.combien()
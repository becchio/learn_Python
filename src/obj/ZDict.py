#!/bin/python3

class ZDict:
    """Classe enveloppe d'un dicitionnaire"""
    def __init__(self):
        """Notre classe n'accepte aucun parametre"""
        self.dictionnaire = {}

    def __getitem__(self, index):
        """Cette méthode spéciale est appelée quand on fait objet[index]
        Elle redirige vers self._dictionnaire[index]=valeur"""

        return self._dictionnaire[index]
    def __setitem__(self, index, valeur):
        """Cette méthode est appelée quand on écrit objet[index] = valeur
        On redirige vers self._dictionnaire[index] = valeur"""

        self._dictionnaire[index] = valeur
    def __delitem__(self, key):
        """del de machin"""
        del self.dictionnaire[key]
    def __contains__(self, item):
        """contient ou pas?"""
        if self.dictionnaire.__contains__(item):
            return "OUI"
        else:
            return 'NON'

    def __len__(self):
        """la taille de ton amour"""
        return len(self.dictionnaire)



zd = ZDict()
zd.dictionnaire['a']=10
zd.dictionnaire['b']=11
zd.dictionnaire['c']=12

print(zd.dictionnaire['a'])
del zd.dictionnaire['a']
print(zd.dictionnaire)

print(zd.__contains__('c'))
print(zd.__contains__('x'))

print(len(zd))
print(zd.__len__())
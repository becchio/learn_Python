#!/bin/python3

class Personne: # définition de notre classe Personne
    """Classe définissant une personne caractérisée par :
    - son nom
    - son prénom
    - son âge
    - son lien de résidence"""

    def __init__(self, nom, prenom, age=""): #Notre méthode constructeur
        """Pour l'instant, on ne va définir qu'un seul attribut"""
        self.nom = nom
        self.prenom = prenom
        self.age = age
        self._lieu_residence = "Paris"
    def _get_lieu_residence(self):
        """Méthode qui sera appelée quand on souhaitera accéder en lecture à l'attribut 'lieu résidence'"""

        print("On accède à l'attribut lieu_residence")
        return self._lieu_residence
    def _set_lieu_residence(self, nouvelle_residence):
        """Méthode qui permet de mettre à jour la residence"""

        print("attention il semble que {} demarrage le {}".format( \
            self.prenom, nouvelle_residence))
        self._lieu_residence=nouvelle_residence
    lieu_residence=property(_get_lieu_residence, _set_lieu_residence)

dupont=Personne("becchio", "greg", "44")
print(dupont)
print(dupont.nom)
print(dupont.prenom)
print(dupont.age)
print(dupont.lieu_residence)

jean = Personne("Pottier", "Damien", "44")
print(jean.nom)
print(jean.prenom)
print(jean.lieu_residence)
jean.lieu_residence="Moscow"
print(jean.lieu_residence)

class Exemple:
    """Un petit exemple de classe"""
    def __init__(self, nom, prenom, age):
        """Exemple de constructeur"""
        self.nom = nom
        self.prenom = prenom
        self.age = age
        self.autre_attribut = "couleur"
    def __del__(self):
        """Méthode appelée quand l'obj et supprimé"""
        print("Fin! Suppression")

    def __str__(self):
        """Méthode permettant d'afficher plus joliment notre objet"""
        return "Personne: nom({}), prénom({}), âge({})".format(self.nom,self.prenom,self.age)
    def __repr__(self):
        """Quand on entre notre objet dans l'intérpréteur"""
        return "Personne: nom({}), prénom({}), âge({})".format(self.nom,self.prenom,self.age)



mon_obj = Exemple("ameno", "o", 27)


print(repr(mon_obj))
print(mon_obj)
print(mon_obj.nom)
print(mon_obj.autre_attribut)
print(mon_obj)
del mon_obj
#!/bin/python3

class Duree:
    """Classe contenant des durées sous la forme d'un nombre de minutes
    et de secondes"""

    def __init__(self, min=0, sec=0):
        """Constructeur de la classe"""
        self.min = min
        self.sec = sec
    def __str__(self):
        """Affichage un peu plus joli de nos object"""
        return "{0:02}:{1:02}".format(self.min, self.sec)
    def __add__(self, other):
        """Que faire de other???"""

        self.sec += other
        print(self.sec)
        if self.sec >=60:
            self.min += self.sec // 60
            self.sec = self.sec % 60
        return self
    def __radd__(self, other):
        """reverse"""
        return self + other
    def __iadd__(self, other):
        return self + other
    def __eq__(self, other):
        """teste si les durées sont égales"""
        return self.sec == other.sec and self.min == other.min
    def __gt__(self, other):
        """Test si self > other"""
        nb_sec1 = self.sec + self.min * 60
        nb_sec2 = other.sec + other.min * 60
        return nb_sec1 > nb_sec2
d = Duree(10, 59)
d1 = Duree(10, 59)
print(d.__eq__(d1))
print(d)

print(d.__add__(4))

print(d.__add__(278))
print(d.sec)
print(d.min)
print(d)
print(d.__radd__(10))
print(4 + d)
d += 4
print(d)
print(d.__eq__(d1))
print(d.__gt__(d1))
#!/bin/python3

class TableauNoir:
    """Classe définissant une surface sur laquelle on peut écrire
    que l'on peut utiliser pour effacer, par jeu de méthodes. Lattribut modifié est 'surface'"""

    def __init__(self):
        """Par défaut, notre surface est vide"""

        self.surface = ""

    def ecrire(self, message_a_ecrire):
        """Méthode permettant d'écrire sur la surface du tableau.
        Si la surface n'est pas vide, on saute une ligneavant de rajouter
        le message à écrire"""
        if self.surface != "":
            self.surface += "\n"
        self.surface += message_a_ecrire

    def lire(self):
        """Cette méthode se charge d'afficher, grâce à print, la
        surface du tableau"""

        print(self.surface)

    def effacer(self):
        """Cette méthode permet d'effacer la surface du tableau"""
        self.surface = ""

tab = TableauNoir()
tab.lire()
tab.ecrire("saluti il mundo")
tab.ecrire("la forma?")
tab.lire()
tab.effacer()
tab.lire()


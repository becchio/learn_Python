#!/bin/python3
import os
import pickle
class Temp:
    """Classe contenant plusieurs attributs, dont un temporaire"""

    def __init__(self):
        """Constructeur de notre objet"""
        self.attr1 = "val1"
        self.attr2 = "val2"
        self.attr_temp = 5

    def __getstate__(self):
        """Renvoie le dictionnaire d'attributs à sérialiser"""
        dict_attr = dict(self.__dict__)
        dict_attr['attr_temp'] = 0
        return dict_attr
    def __setstate__(self, state):
        """Fait des opérations lors de la désérialisation"""
        state["attr_temp"] = 10
        self.__dict__ = state


temp = Temp()
fichier_temp = open("temp", "wb")  # On écrase les anciens scores
mon_pickler = pickle.Pickler(fichier_temp)
mon_pickler.dump(temp)
fichier_temp.close()

if os.path.exists("temp"):  # Le fichier existe
    # On le récupère
    fichier_temp = open("temp", "rb")
    mon_depickler = pickle.Unpickler(fichier_temp)
    temp = mon_depickler.load()
    print(temp.__getstate__())
    print(temp.attr_temp)
    fichier_temp.close()
else:  # Le fichier n'existe pas
    print("pas de fichier temp")

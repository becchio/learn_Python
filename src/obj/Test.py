#!/bin/python3

class Test:
    """Une classe de test tout simplement"""
    def __init__(self):
        """On définit dans le constructeur un unique attribut"""
        self.mon_attribut="ok"
    def afficher():
        """Fct chergée d'afficher qqch"""
        print("On affiche la même chose")
        print("données obj ou classe")
    afficher = staticmethod(afficher)

    def afficher_attribut(self):
        """Méthode adichant l'attibut 'on_attribut'"""
        print("Mon attribut est {0}.".format(self.mon_attribut))

t=Test()
print(t.afficher())
t.afficher_attribut()
print(dir(t))
print(t.__dict__)
print(t.__sizeof__)
print(t.__doc__)
print(t.__class__)
print(t.__str__)
t.__dict__["mon_attribut"]='KAK'
print(t.afficher_attribut())
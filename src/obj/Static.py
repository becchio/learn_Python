#!/bin/python3

class Static:
    """Une classe de test tout simplement"""
    def afficher(self):
        """Fonction chargée d'affichier quelque choser"""
        print("On affiche la même chose")
        print("peu importe les données de l'objet ou de la classe.")
    afficher = staticmethod(afficher)

Static.afficher(Static)
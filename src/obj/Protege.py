#!/bin/python3

class Protege:
    """Classe possédant une méthode particulière d'accès à ses attributs:
    Si l'attribut n'est pas trouvé, on affiche une alerte et renvoie None"""

    def __init__(self):
        """On crée quelques attributs par défaut"""
        self.a = 1
        self.b = 2
        self.c = 3
    def __getattr__(self, nom):
        """Si Pytho ne trouve pas l'attribut nommé nom, il appelle
        cette méthode. On affiche une alerte"""

        return "Alerte ! Il n'y a pas d'attribut {} ici !".format(nom)
    def __setattr__(self, nom_attr, val_attr):
        """Méthode appelée quand on fait objet.nom_attr = val_attr.
        On se charge d'enregistrer l'objet"""
        object.__setattr__(self, nom_attr, val_attr)
        return 1
    def __delattr__(self, nom_attr):
        """On ne peut supprimer d'attribut, on lève l'exception AttributeError"""
        #raise AssertionError('Vous ne pouvez supprimer aucun attribut de cette classe')
        object.__delattr__(self, nom_attr)
        print(nom_attr + " supprimé avec succès")



pro = Protege()
print(pro.a)

print(pro.b)

print(pro.c)

print(pro.d)

pro.d = 10

print(pro.d)

del pro.d
print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
obj = Protege()
getattr(obj, "a")
setattr(obj, "a","aaa")
delattr(obj,'a')
print(hasattr(obj, 'a'))
print(obj.a)
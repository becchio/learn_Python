#!/bin/python3

ma_liste1 = [1, 2, 3]
ma_liste2 = ma_liste1
ma_liste2.append(4)
print(ma_liste2)
print(ma_liste1)

ma_liste3 = list(ma_liste2)
print(ma_liste2)
del ma_liste2[2]
print(ma_liste2)
print(ma_liste3)

mon_dict = {"a":"a", "b":"b", "c":"c"}
mon_dict2 = dict(mon_dict)

del mon_dict['a']

print(mon_dict)
print(mon_dict2)

m_l1 = [1, 2]
m_l2 = [1, 2]

print(m_l1 == m_l2)
print(m_l1 is m_l2)
#!/bin/python3

i = 4

def inc_i():
    """Fonction chergée d'incrémenter i de 1"""
    global i # Python recherche i en dehors de l'espace local de la fonction

    i += 1
print(i)

inc_i()
print(i)

#!/usr/bin/python3.5

import hashlib
from getpass import getpass

chaine_mot_de_passe = b"azerty"
mdp = hashlib.sha1(chaine_mot_de_passe).hexdigest()

verr = True
while verr:
    entre = getpass("Tapez le mot de passe: ")

    entre = entre.encode()

    entre_chiffre = hashlib.sha1(entre).hexdigest()
    if entre_chiffre == mdp:
        verr = False
    else:
        print("Mot de passe incorrect")

print("Mot de passe accepté")
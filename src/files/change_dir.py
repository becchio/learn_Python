#!/bin/python3

import os
print(os.getcwd())
os.chdir("../test")

print(os.getcwd())

open("fichier.txt", "w")
my_file = open("fichier.txt", "w")
my_file.write("Premier test\net le chat")
my_file.close()

mi_file = open("fichier.txt", "r")
print(mi_file.read())

mi_file.close()

# manipulationh de fichiers en toute sécurité
with open("fichier.txt", "r") as mon_fichier:
    texte = mon_fichier.read()

print(texte)
print(mon_fichier.closed)
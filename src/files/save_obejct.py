#!/bin/python3

import pickle

score = {
    "j 1": 5,
    "j 2": 35,
    "j 3": 20,
    "j 4": 2,
}

with open("donnees", "wb") as fichier:
    mon_pick = pickle.Pickler(fichier)
    mon_pick.dump(score)

with open("donnees", "rb") as fichier:
    mon_depick = pickle.Unpickler(fichier)
    score_recup = mon_depick.load()

print(score_recup)
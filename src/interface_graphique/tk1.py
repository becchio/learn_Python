#!/usr/bin/python3

"""Premier exemple avec Tkinter.

On crée une fenêtre simple qui souhaite la bienvenue à l'utilisateur.

"""

# On importe Tkinter
from tkinter import *

# On crée une fenêtre, racine de notre interface
fenetre = Tk()

# organisation des widgets
#cadre = Frame(fenetre, width=10, height=10, borderwidth=0)
#cadre.pack(fill=Y)

#message = Label(cadre, text="Notre fenêtre")
#message.pack(side="top", fill=Y)

# cadre avec titre
cadre2 = LabelFrame(fenetre, text="Titre du cadre")
cadre2.pack(fill=BOTH)

# On crée un label (ligne de texte) souhaitant la bienvenue
# Note : le premier paramètre passé au constructeur de Lable est notre
# interface racine
champ_label = Label(fenetre, text="Salut les Zér0s !")
#champ_label.pack()

# On affiche le label dans la fenêtre
champ_label.pack()

# Boutton quitter
bouton_quitter = Button(fenetre, text="Quitter", command=fenetre.quit)
bouton_quitter.pack()

# texte ligne
var_texte = StringVar()
ligne_texte = Entry(fenetre, textvariable=var_texte, width=30)
ligne_texte.pack()

# texte à plusieurs champs
var_texte = StringVar()
ligne_texte = Text(fenetre)
ligne_texte.pack()

# cases à cocher
var_case = IntVar()
case = Checkbutton(fenetre, text="Ne plus poser cette question", variable=var_case)
case.pack()

# bouton radio
var_choix = StringVar()
choix_rouge = Radiobutton(fenetre, text="Rouge", variable=var_choix, value="rouge")
choix_vert = Radiobutton(fenetre, text="Vert", variable=var_choix, value="vert")
choix_bleu = Radiobutton(fenetre, text="Bleu", variable=var_choix, value="Bleu")

choix_rouge.pack()
choix_vert.pack()
choix_bleu.pack()

# liste
liste = Listbox(fenetre)
liste.pack()
liste.insert(END, "Pierre")
liste.insert(END, "Feuille")
liste.insert(END, "Ciseau")




# On démarre la boucle Tkinter qui s'interrompt quand on ferme la fenêtre
fenetre.mainloop()


#!/usr/bin/python3.5

from fractions import Fraction

un_demi = Fraction(1, 2)
print(un_demi)

un_quart = Fraction("1/4")
print(un_quart)

autre_fraction = Fraction(-5, 30)
print(autre_fraction)
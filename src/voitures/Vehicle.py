#!/usr/bin/python3.5

class Vehicle:
    kind = 'car'

    def __init__(self, manufacturer, model):
        self.manufacturer = manufacturer
        self.model_name = model

    @property
    def name(self):
        return "%s %s" % (self.manufacturer, self.model_name)

    def __repr__(self):
        return "<%s>" % self.name

car = Vehicle('Toyota', 'Corolla')
print(car, car.kind)
print(car.model_name)
car2 = Vehicle("Lada", "Da")
Vehicle.kind = 'scrap'
print(car.kind)
print(car2.kind)
car.kind='rien'
print(car.kind)
print(car2.kind)
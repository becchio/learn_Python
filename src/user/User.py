#!/usr/bin/pyton3

class User:
    def __init__(self, full_name='greg', birthday='20010101'):
        self.name = full_name
        self.birthday = birthday # yyyymmdd

        # Extract first and last names
        name_pieces = full_name.split(" ", 1)
        if len(name_pieces) == 1:
            self.first_name = name_pieces[0]
        elif len(name_pieces) == 0:
            self.first_name = ""
            self.last_name = ""
        else:
            self.last_name = name_pieces[1]

u1 = User("Grégory Becchio de la vida del sol")
u1.f_name = 'Greg'
u1.l_name = "Becchio"
u1.birthday = "19731013"
print(u1.f_name)
print(u1.l_name)
print(u1.birthday)
print(u1.first_name)
print(u1.last_name)
print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
u2 = User()
u2.f_name = 'Max'
u2.l_name = "Lamenace"
u2.birthday = "19910227"

print(u2.f_name)
print(u2.l_name)
print(u2.birthday)


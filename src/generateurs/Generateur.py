#!/bin/python3

def mon_generateur():
    """Notre premier générateur. Il va simplement renvoyer 1, 2 et 3"""

    yield 1
    yield 2
    yield 3

print(mon_generateur)
print(mon_generateur())

mon_iterateur = iter(mon_generateur())
try:
    print(mon_iterateur)
    print(next(mon_iterateur))
    print(next(mon_iterateur))
    print(next(mon_iterateur))
    print(next(mon_iterateur))
except StopIteration:
    print("fin")

for nombre in mon_generateur(): # on execute la fonction
    print(nombre)
def intervalle(borne_inf, borne_sup):
    """Générateur parcourant la série des entiers entre borne_inf et borne_sup

    Note: borne_inf doit être inférieure à borne_sup"""

    borne_inf += 1
    while borne_inf < borne_sup:
        valeur_recue = (yield borne_inf)
        if valeur_recue is not None: # Notre générateur a reçu quelque chose
            borne_inf = valeur_recue
        borne_inf += 1
print("############################")
for nombre in intervalle(5, 10):
    print(nombre)
print("############################")
generateur = intervalle(5, 20)
for nombre in generateur:
    if nombre > 17:
        generateur.close() # Interruption de la boucle
    print(nombre)

print("############################################")

generateur = intervalle(10, 25)
for nombre in generateur:
    if nombre == 15: # On saute à 20
        generateur.send(20)
    print(nombre, end="<<<")

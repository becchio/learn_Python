# -- FILE: features/steps/example_steps.py
import os
from behave import given, when, then, step

@given('we have behave installed')
def step_impl(context):
    temp_version = os.popen("behave --version").read()
    assert os.popen("behave --version").read() == "behave 1.2.5\n"

@when('we implement {number:d} tests')
def step_impl(context, number):  # -- NOTE: number is converted into integer
    assert number > 1 or number == 0
    context.tests_count = number

@then('behave will test them for us!')
def step_impl(context):
    assert context.failed is False
    assert context.tests_count >= 0


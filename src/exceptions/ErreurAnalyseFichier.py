#!/bin/python3

class ErreurAnalyseFichier(Exception):
    """Cette exception est levée quant un efichier (de configuration)
    n'a pas pu être analysé

    Attributs :
        fichier -- le nom du fichier posant problème
        ligne -- le numéro de la ligne posant problème
        message -- le problème proprement dit"""

    def __init__(self, fichier, ligne, message):
        """Constructeur de notre exception"""
        self.fichier = fichier
        self.ligne = ligne
        self.message = message
    def __str__(self):
        """Affichage de l'exception"""
        return "[{}:{}]: {}".format(self.fichier, self.ligne, \
                                    self.message)

raise ErreurAnalyseFichier('plop.conf', 34, "Il manque une parenthèse à la fin de l'expression")
class TestAgent:
    def setup_method(self):
        self.agent = script.Agent(3)

    # - Agent :
    #   - modifier un attribut position
    def test_set_position(self):
        self.AGENT.position = 5
        assert self.agent.position == 5

    #   - récupérer un attribut position
    def test_get_position(self):
        assert self.agent.position == 3

    #   - assigner un dictionnaire en tant qu'attributs
    def test_set_agent_attributes(self):
        agent = script.Agent(3, agreeableness=-1)
        assert agent.agreeableness == -1
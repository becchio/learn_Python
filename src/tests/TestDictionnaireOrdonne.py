import sys, os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../../')
import src.tp_dico.DictionnaireOrdonne as script
import pytest


class TestDictionnaireOrdonne:
    def setup_method(self):
        self.vide = script.DictionnaireOrdonne()
        self.dico = script.DictionnaireOrdonne({"a":"lettre A", "b":"Lettre b", "c":"Lettre c"})
        self.predico = script.DictionnaireOrdonne(a=1, b=2, c=3)

    def test_init(self):
        assert self.vide.__str__() == "{}"
        print()
        assert len(self.dico) == len({'b': 'Lettre b', 'a': 'lettre A', 'c': 'Lettre c'})
        assert self.dico['a'] == "lettre A"
        assert self.predico["a"] == 1

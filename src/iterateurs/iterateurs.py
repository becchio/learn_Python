#!/bin/python3

ma_liste = [1, 2, 3]
for element in ma_liste:
    print(element)
ma_chaine = "test"
iterateur_de_ma_chaine = iter(ma_chaine)
print(iterateur_de_ma_chaine)

try:
    next(iterateur_de_ma_chaine)
    next(iterateur_de_ma_chaine)
    next(iterateur_de_ma_chaine)
    next(iterateur_de_ma_chaine)
    next(iterateur_de_ma_chaine)
except StopIteration as si:
    print("Y en a plus!!!")
    print(si.args)

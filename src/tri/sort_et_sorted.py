#!/bin/python3

prenoms = [
    "Jacques",
    "Laure",
    "André",
    "Victoire",
    "Albert",
    "Sophie"
]

prenoms.sort()
print(prenoms)

prenoms = [
    "Jacques",
    "Laure",
    "André",
    "Victoire",
    "Albert",
    "Sophie"
]
a = sorted(prenoms)
print(a)
print(prenoms)

b = sorted([1, 8, -2, 15, 9])
print(b)

c = sorted(["1", "8", "-2", "15", "9"])
print(c)

d = sorted(["1", "2", 3])
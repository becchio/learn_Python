#!/bin/python3
from operator import itemgetter
from operator import attrgetter

class Etudiant:
    """Classe représentant un étudiant

    On représente un étudiant par son prénom (attribut prenom), son âge
    (attribut âge) et sa note moyenne (attribut moyennen entre 0 et 20).

    Paramètres du constructeur
        prenom -- le prénom del'étudiant
        age -- l'âge de l'étudiant
        moyenne -- la moyenne de l'étudiant


    """

    def __init__(self, prenom, age, moyenne):
        self.prenom = prenom
        self.age = age
        self.moyenne = moyenne

    def __repr__(self):
        return "<Etudiant {} (âge={}, moyenne={})>".format(self.prenom, self.age, self.moyenne)

etudiants = [
    Etudiant("Clément", 11, 16),
    Etudiant("Charles", 12, 15),
    Etudiant("Oriane", 13, 18),
    Etudiant("Thomas", 14, 12),
    Etudiant("Damien", 15, 15),
]

notes = [
    1,
    2,
    3,
    4,
]
print(etudiants)
b = sorted(etudiants, key=lambda etudiant: etudiant.age, reverse=True)
print(b)

etudiants = [
    ("Clément", 11, 16),
    ("Charles", 12, 15),
    ("Oriane", 13, 18),
    ("Thomas", 14, 12),
    ("Damien", 15, 15),
]
c = sorted(etudiants, key=itemgetter(2), reverse=True)
print("#################################################")
print(c)

etudiants = [
    Etudiant("Clément", 11, 16),
    Etudiant("Charles", 12, 15),
    Etudiant("Oriane", 13, 18),
    Etudiant("Thomas", 14, 12),
    Etudiant("Damien", 15, 15),
]

d = sorted(etudiants, key=attrgetter("age", "moyenne"))
print(d)

e = sorted(etudiants, key=attrgetter("moyenne", "age"), reverse=True)
print(e)
#!/bin/python3
from operator import attrgetter
class LigneInventaire:

    """Classe représentant uen ligne d'un inventaire de vente

    Attributs attendus par le constructeur :
        produit -- le nom du produit
        prix -- le prix unitaire du produit
        quantite -- la quantité vendue du produit.

    """

    def __init__(self, produit, prix, quantite):
        self.produit = produit
        self.prix = prix
        self.quantite = quantite

    def __repr__(self):
        return "<Ligne d'inventaire {} ({}x{})>".format(self.produit, self.prix, self.quantite)

# création de l'inventaire
inventaire = [
    LigneInventaire("pomme rouge", 1.2, 19),
    LigneInventaire("orange", 1.4, 24),
    LigneInventaire("banane", 0.9, 21),
    LigneInventaire("poire", 1.2, 24),
]

a = sorted(inventaire, key=attrgetter('prix', 'quantite'))
print(a)

b = sorted(inventaire, key=attrgetter('produit', 'prix'))
print(b)

inventaire.sort(key=attrgetter("prix"))
c = sorted(inventaire, key=attrgetter("quantite"), reverse=True)
print(c)


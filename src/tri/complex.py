#!/bin/python3

etudiants = [
    ("Clément", 14, 16),
    ("Charles", 12, 15),
    ("Oriane", 14, 18),
    ("Thomas", 11, 12),
    ("Damien", 12, 15)
]

a = sorted(etudiants)

print(etudiants)
print(a)

doubler = lambda x: x * 2
print(doubler)

a = doubler(8)
print(a)

b = sorted(etudiants, key=lambda colonnes: colonnes[2])
print(b)
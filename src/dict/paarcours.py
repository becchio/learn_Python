#!/bin/python3

fruits = {
    "pommes":21,
    "melons":3,
    "poires":31
}
for valeur in fruits.values():
        print(valeur)

for cle in fruits:
    print(cle)

for cle in fruits.keys():
    print(cle)

print(fruits.keys)

if 21 in fruits.values():
    print("il existe 21 fruits")

# enumerate avec la méthode items
for cle, valeur in fruits.items():
    print(type(cle))
    print(type(valeur))
    print("il y a {} {}.".format(valeur, cle))

def fct_inconnue(**parametres_nommes):
    """Fct permet de récupérer les params nommés d'un dico"""
    print("param nommés: {}.".format(parametres_nommes))

fct_inconnue()
fct_inconnue(p=4, j=8, a="c'est une chanson, qui nous ressemble...", c=fruits)

def fct_inc(*en_liste, **en_dict):
    print("en liste {} | en dict {}".format(en_liste, en_dict))

fct_inc()
fct_inc(p=4, j=8, a="c'est une chanson, qui nous ressemble...", c=fruits)

param={"sep":" >>> ", "end":" -\n"}
print("voici", "un", "cercle", **param)

del fruits["pommes"]
print(fruits)
combien=fruits.pop('poires')
print(combien)
print(fruits)

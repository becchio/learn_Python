#!/usr/bin/python3.5

class Personne:
    """Classe définissant une personne.

    Elle possède comme attributs :
    nom -- le nom de la personne
    prenom -- son prénom
    age -- son âge
    lieu_residence -- son lieu de résidence

    Le nom et le prénom doivent être passés au constructeur."""

    def __new__(cls, nom, prenom):
        print("Appel de la méthode __new__ de la classe {}".format(cls))
        # On laisse le travail à object
        return object.__new__(cls)

    def __init__(self, nom, prenom):
        """Constructeur de notre personne."""
        print("Appel de la méthode __init__")
        self.nom = nom
        self.prenom = prenom
        self.age = 23
        self.lieu_residence = "Lyon"

p = Personne("greg", "becchio")
print("nom:", p.nom+",", "prenom:", p.prenom+",", "age:", str(p.age)+",", "lieu de résidence", p.lieu_residence)

Personne = type("Personne", (), {})
print(Personne)
greg = Personne()
print(greg)
print(dir(greg))

def creer_personne(personne, nom, prenom):
    """
    La fonction qui jouera le rôle de constructeur pour notre classe
    Personne.

    Elle prend en paramètre, outre la personne :
    nom -- son nom
    prenom -- son prenom

    :param peronne:
    :param nom:
    :param prenom:
    :return:
    """

    personne.nom = nom
    personne.prenom = prenom
    personne.age = 21
    personne.lieu_residence = "Lyon"

def presenter_personne(personne):
    """
    Fonction présentant la personne.
    Elle affiche son prénom et son nom

    :param personne:
    :return: prenom + nom
    """

    print("{} {}".format(personne.prenom, personne.nom))

# dictionnaire de méthodes
methodes = {
    "__init__": creer_personne,
    "presenter": presenter_personne
}

# Création dynamique de la classe
Personne = type("Personne", (), methodes)
greg = Personne('Grégory', 'Becchio')
print(greg.prenom, greg.nom, greg.age, greg.lieu_residence)
print(dir(type(methodes)))


#!/usr/bin/python3.5
class MaMetaClasse(type):

    """
    Exemple d'une métaclasse.
    """

    def __new__(metacls, nom, bases, dict):
        """
        Création de notre classe.

        :param nom:
        :param bases:
        :param dict:
        :return:
        """

        print("On crée la classe {}".format(nom))
        return type.__new__(metacls, nom, dict)

class MaClasse(metaclass=MaMetaClasse):
    pass

#!/bin/python3
import A
class B(A):
    """Classe B, qui hérite de A.
    Elle reprenf les mêmes méthodes et attributs (dans cet exemple, la classe
    A ne possède de toute façon ni méthode ni attribut)"""

    pass
b = B()

#!/bin/python3

class A:
    """Classe A, pour illustrer notre exemple d'heritage"""
    pass # On laisse la définition vide, ce n'est qu'un exemple
class B(A):
    """Classe B, qui hérite de A.
    Elle reprenf les mêmes méthodes et attributs (dans cet exemple, la classe
    A ne possède de toute façon ni méthode ni attribut)"""

    pass

b = B()
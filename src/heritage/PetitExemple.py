#!/bin/python3

class Personne:
    """Classe représentant une personne"""
    def __init__(self, nom):
        """Constructeur de notre classe"""
        self.nom = nom
        self.prenom = "Martin"
    def __str__(self):
        """Méthode appelée lors d'une conversion de l'objet en chaine"""
        return "{0} {1}".format(self.prenom, self.nom)

class AgentSpecial(Personne):
    """Classe définissant un agent spécial
    Elle hérite de la classe Peronne"""

    def __init__(self, nom, matricule):
        """Un agent se définit par son nom et son matricule"""
        Personne.__init__(self, nom)
        self.matricule = matricule
    def __str__(self):
        """Méthode appelée lors d'une conversion de l'objet en chaîne"""
        return "Agent {0}, matricule {1}".format(self.nom, self.matricule)
    def __setattr__(self, key, value):
        """Méthode appelée quand on fait objet.attibut = valeur"""
        print("Attention, on modifie l'attribut {0} de l'objet avec la valeur >>>{1}<<<!".format(key, value))
        object.__setattr__(self, key, value)

agent = AgentSpecial("Fisher", "18327-121")
print(agent.nom)
print(agent)
print(agent.prenom)
print(issubclass(AgentSpecial, Personne))
print(issubclass(AgentSpecial, object))
print(issubclass(Personne, AgentSpecial))

print(isinstance(agent, AgentSpecial))
print(isinstance(agent, Personne))
print(isinstance(agent, A))

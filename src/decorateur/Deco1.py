#!/usr/bin/python3.5
def obsolete(fonction_origine):
    """Décorateur levant une exception pour noter que la fonction_origine
    est obsolète"""

    def fonction_modifiee():
        raise RuntimeError("la fonction {0} est obsolète !".format(fonction_origine))

    return fonction_modifiee

@obsolete
def salut():
    print("salut!")

salut()
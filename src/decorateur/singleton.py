#!/usr/bin/python3.5

def singleton(classe_definie):
    instances = {} # Dictionnaire de nos instances singletons
    def get_instance():
        if classe_definie not in instances:
            # On crée notre premier objet de classe_definie
            instances[classe_definie] = classe_definie()
        return instances[classe_definie]
    return get_instance
    print(instances)

@singleton
class Test:
    pass

class NonTest:
    pass

a = Test()
b = Test()
c = NonTest()

print(a is b)
print(a is not c)
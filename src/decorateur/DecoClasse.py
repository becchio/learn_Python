#!/usr/bin/python3.5

def decorateur(classe):
    print("Definition de la classe {0}".format(classe))
    return classe

def decorateur2(classe):
    print("Definition de la classe {0}".format(classe))
    return classe

@decorateur
class Test:
    pass

@decorateur2
@decorateur
class Ifsi:
    pass